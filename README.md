# YouTodo

Minimalistic Todo app for Ubuntu Touch

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/youtodo.christianpauly)

### How to build

1. Install clickable as described here: https://github.com/bhdouglass/clickable

2. Clone this repo:
```
git clone https://gitlab.com/ChristianPauly/youtodo
cd youtodo
```

3. Run tests
```
clickable test
```

4. Build with clickable
```
clickable build
```
