import QtQuick 2.9
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import QtQuick.LocalStorage 2.0
import "../YouTodo.js" as YouTodo

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'youtodo.christianpauly'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property var tasks: new YouTodo.Tasks(root)

    PageStack {
        id: mainStack
        function pushPage ( page ) {
            var path = "./pages/%1Page.qml".arg( page )
            mainStack.push( Qt.resolvedUrl( path ) )
        }
        Component.onCompleted: mainStack.pushPage ( "List" )
    }
}
